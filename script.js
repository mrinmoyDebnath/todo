const inputItem = document.getElementById('itemName');
const addBtn = document.getElementById('add-list');
const list = document.getElementById('todo-items');

const listOfItems = [];
let totalItems=0;

addBtn.addEventListener('click', event=>{
    if(!inputItem.value) console.log('add first');
    else addItem(inputItem.value);
})

function addItem(item){
    listOfItems.push({
        name: item,
        completed: false,
        id: totalItems
    })
    totalItems+=1
    console.log(listOfItems)
    render();
}

list.addEventListener('click', event=>{
    if(event.target.className==='close'){
        deletedItem = event.target.parentElement.firstChild.id
        console.log(deletedItem)
        listOfItems.forEach((item, index) => {
            if(item.id==deletedItem){
                listOfItems.splice(index, 1)
            }
        });
        render();
    }
    if(event.target.classList[0]==='check'){
        completedTask(event.target.id)
    }
});

function completedTask(id){
    console.log(id)
    listOfItems.forEach(item => {
        if(item.id==id){
            item.completed = !item.completed;
        }
    });
    render();
}

function render(){
    while(list.firstChild){
        list.removeChild(list.firstChild);
    }
    listOfItems.forEach((item, index)=>{
        const newItem = document.createElement('li');
        const newDiv = document.createElement('div');
        newDiv.classList.add('list-item');
        newDiv.classList.add('pending');
        newDiv.innerHTML = `<div class="check" id='${item.id}'><input type='checkbox'></div> <h2>${item.name}</h2> <div class='close'>x</div>`;
        if(item.completed){
            newDiv.firstChild.firstChild.checked = true;
            newDiv.firstChild.classList.add('check-done');
            newDiv.classList.add('striked')
            newDiv.classList.add('done');
        }else{
            newDiv.firstChild.firstChild.checked = false;
            newDiv.firstChild.classList.remove('check-done');
            newDiv.classList.remove('striked')
            newDiv.classList.remove('done');
        }
        newItem.appendChild(newDiv);
        list.appendChild(newItem);
    })
}